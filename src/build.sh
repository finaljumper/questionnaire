#!/usr/bin/env bash
[ -d venv ] && rm -Rf venv
mkdir venv && cd venv
virtualenv -p python3 env
cd ..
source venv/env/bin/activate
pip install -r requirements.txt
python manage.py migrate
echo "Build success"
python manage.py runserver 0.0.0.0:80
